//
//  ListViewController.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/21/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit
import SDWebImage

class ListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    var pageIndex: Int = 0
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    
    func MovieList(check: Int) {
        print(check)
    }
    
    static var idMVDetail: Int?
    static var checkMVDetail: Bool?
    
   @IBOutlet weak var tableView: UITableView!
    
    var po:[MovieDetail] = [MovieDetail]()
    {
        didSet
        {
            tableView.reloadData()
        }
    }
    
    var IDMovie: Int?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ListViewController.checkMVDetail = false
        self.navigationItem.title = "List Movie"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        tableView.backgroundColor = .black
        tableView.delegate = self
        tableView.dataSource = self
        tableView.reloadData()
        //tableView.separatorStyle = .none
        print(ViewController.CheckID)
        loadMoreData()
        AddPullToRefresh()
    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        super.viewWillDisappear(true)
//        
//    }
    
    func AddPullToRefresh (){
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        refreshControl.attributedTitle = NSAttributedString(string: "", attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        tableView.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc func refresh() {
        pageIndex = 0
        po.removeAll()
        loadMoreData()
        refreshControl.endRefreshing();
    }
    
    func setPoMovieList(pageIndex : Int) {
        print(1)
        API().getJSONResponse(baseUrl: API.urlBasePo, param: API.getParamMovieList(page: pageIndex)){
            rs in
            //self.po.removeAll()
            for i in rs
            {
                //print(i.tittle)
                //print(i.backdrop_path)
                self.po.append(i)
            }
            print("a")
        }
    }
    
    func setNowMovieList(pageIndex : Int) {
        print(1)
        API().getJSONResponse(baseUrl: API.urlBaseNow, param: API.getParamMovieList(page: pageIndex)){
            rs in
            //self.po.removeAll()
            for i in rs
            {
                //print(i.tittle)
                //print(i.backdrop_path)
                self.po.append(i)
            }
            print("a")
        }
    }
    
    func setTopMovieList(pageIndex : Int) {
        print(1)
        API().getJSONResponse(baseUrl: API.urlBaseTop, param: API.getParamMovieList(page: pageIndex)){
            rs in
            //self.po.removeAll()
            for i in rs
            {
                //print(i.tittle)
                //print(i.backdrop_path)
                self.po.append(i)
            }
            print("a")
        }
    }
    
    func setUpMovieList(pageIndex : Int) {
        print(1)
        API().getJSONResponse(baseUrl: API.urlBaseUp, param: API.getParamMovieList(page: pageIndex)){
            rs in
            //self.po.removeAll()
            for i in rs
            {
                //print(i.tittle)
                //print(i.backdrop_path)
                self.po.append(i)
            }
            print("a")
        }
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(po.count)
        return po.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellList", for: indexPath) as! cellList
        
        if po.count > 0{
            let earchIv = self.po[indexPath.row]
            cell.name.text = "Name: "+String(earchIv.tittle!)
            cell.date.text = "Date: "+String(earchIv.release_date!)
            cell.vote.text = "Vote: "+String(earchIv.vote_count!)
            cell.cosmos.rating = Double(earchIv.vote_average!)
            cell.voteStar.text = "("+String(earchIv.vote_average!)+")"
            
            if (earchIv.poster_path != nil) {
                let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.poster_path! as? String
                print(stringiv)
                cell.img?.sd_setImage(with: URL(string: stringiv!), placeholderImage: UIImage(named: "meow.jpeg"))
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let lastItem = po.count - 1
        if indexPath.row == lastItem
        {
            loadMoreData()
        }
    }
    func loadMoreData() {
        pageIndex = pageIndex + 1
        if let id:Int  = ViewController.CheckID {
            if id == 0{
                setPoMovieList(pageIndex: pageIndex)
            }else if id == 1{
                setNowMovieList(pageIndex: pageIndex)
            }else if id  == 2 {
                setTopMovieList(pageIndex: pageIndex)
            }else if id  == 3 {
                setUpMovieList(pageIndex: pageIndex)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(1)
        getIDMovieDetail(id: po[indexPath.row].id!)
    }
    
    func getIDMovieDetail(id: Int) {
        print(id)
        ListViewController.idMVDetail = id
        ListViewController.checkMVDetail = true
        ViewController.checkMVDetail = false
        let mvc = self.storyboard?.instantiateViewController(withIdentifier: "moviedetail") as! MovieViewController
        self.present(mvc, animated: true, completion: nil)
        
    }
//    override func viewWillAppear(_ animated: Bool) {
//        super.viewWillAppear(true)
//        
//    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
}


