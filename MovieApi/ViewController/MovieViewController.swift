//
//  MovieViewController.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 8/3/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit

class MovieViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var isTrackingPanLocation = false
    var panGestureRecognizer: UIPanGestureRecognizer!
    
    var po:[ShowMovieDetail] = [ShowMovieDetail]()
    {
        didSet
        {
            tableView.reloadData()
        }
    }
    
    var trailer:[ShowTrailerDetail] = [ShowTrailerDetail]()
    {
        didSet
        {
            tableView.reloadData()
        }
    }
    
    var trailerID:[String] = [String]()
    {
        didSet
        {
            tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print(po.count)
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if po.count > 0
        {
            if indexPath.row == 0
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! InfoCell
                
                guard let stringiv = "https://image.tmdb.org/t/p/w1280" + po[0].image! as? String else { }
                print(stringiv)
                cell.imgInfoCell?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
                //cell.backgroundColor = .black
//                cell.layer.cornerRadius = 20
//                cell.layer.shadowColor = UIColor.gray.cgColor
//                cell.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
//                cell.layer.shadowRadius = 12.0
//                cell.layer.shadowOpacity = 0.7
                
                return cell
            }
            
            if indexPath.row == 1
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellList1", for: indexPath) as! cellList_1
                
                cell.textCell.text = po[0].overview!
                
                //cell.backgroundColor = .black
//                cell.layer.cornerRadius = 10
//                cell.contentView.layoutMargins.left = 5
//                cell.contentView.layoutMargins.right = 5
//                cell.contentView.layoutMargins.top = 5
                
                return cell
            }
            if indexPath.row == 2
            {
                let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell2", for: indexPath) as! TrailerTableCell
                
                cell.TrailerCollection.delegate = self
                cell.TrailerCollection.dataSource = self
                cell.TrailerCollection.reloadData()
                
                return cell
            }
        }
        let cell = UITableViewCell()
        
        //cell.backgroundColor = .black
        
        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        print(ListViewController.idMVDetail)
        
        if ListViewController.checkMVDetail == true
        {
            let id = ListViewController.idMVDetail!
            API().getMovieDetailJSONResponse(baseUrl: API.getURLMovieDetail(id: String(id)), param: API.getDefaultParam())
            {
                rs in
                self.po.removeAll()
                for i in rs
                {
                    //print(i.tittle)
                    //print(i.image!)
                    self.po.append(i)
                    print(i)
                }
                print("a")
            }
            
            API().getTrailerJSONResponse(baseUrl: API.getTrailerKeyURL(id: String(id)), param: API.getDefaultParam())
            {
                rs in
                self.trailer.removeAll()
                for i in rs
                {
                    //print(i.tittle)
                    //print(i.image!)
                    self.trailer.append(i)
                    self.trailerID.append(i.key!)
                    print(i)
                }
                print("a")
            }
        }
        else
        {
            if ViewController.checkMVDetail == true
            {
                let id = ViewController.idMVDetail!
                API().getMovieDetailJSONResponse(baseUrl: API.getURLMovieDetail(id: String(id)), param: API.getDefaultParam())
                {
                    rs in
                    self.po.removeAll()
                    for i in rs
                    {
                        //print(i.tittle)
                        //print(i.image!)
                        self.po.append(i)
                        print(i)
                    }
                    print("a")
                }
                
                API().getTrailerJSONResponse(baseUrl: API.getTrailerKeyURL(id: String(id)), param: API.getDefaultParam())
                {
                    rs in
                    self.trailer.removeAll()
                    for i in rs
                    {
                        //print(i.tittle)
                        //print(i.image!)
                        self.trailer.append(i)
                        self.trailerID.append(i.key!)
                        print(i)
                    }
                    print("a")
                }
            }
            
        }
        
        tableView.allowsSelection = false
        tableView.delegate = self
        tableView.dataSource = self
        tableView.layer.cornerRadius = 10
        tableView.layer.masksToBounds = true
        
        tableView.bounces = false
        panGestureRecognizer = UIPanGestureRecognizer(target: self,
                                                      action: #selector(self.panRecognized))
        panGestureRecognizer.delegate = self as? UIGestureRecognizerDelegate
        self.tableView.addGestureRecognizer(panGestureRecognizer)
    }
    
    
    @objc public func panRecognized(recognizer: UIPanGestureRecognizer) {
            let panOffset = recognizer.translation(in: self.view)
            
            // determine offset of the pan from the start here.
            // When offset is far enough from table view top edge -
            // dismiss your view controller. Additionally you can
            // determine if pan goes in the wrong direction and
            // then reset flag isTrackingPanLocation to false
        self.view.frame.origin = panOffset
        
        print(panOffset.y)
        
        if recognizer.state == .ended{
            let velocity = recognizer.velocity(in: self.view)
            if velocity.y >= 1500 || panOffset.y > 3*self.view.frame.size.height/5
            {
                // dismiss the view
                self.dismiss(animated: true, completion: nil)
            }
            else
            {
                //return to the original position
                UITableView.animate(withDuration: 0.3, animations: {
                    self.dismiss(animated: true, completion: nil)
                    //self.view.frame.origin = CGPoint(x: 0.0, y: 0.0)
                })
            }
        }
    }
    
    public func gestureRecognizer(gestureRecognizer: UIGestureRecognizer,
                                  shouldRecognizeSimultaneouslyWithGestureRecognizer
        otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 0
        {
            return tableView.frame.size.height/4
        }
        else if indexPath.row == 1
        {
            return 2*tableView.frame.size.height/4
        }
        else if indexPath.row == 2
        {
            return tableView.frame.size.height/4
        }
        print(tableView.frame.size.height)
        return 30.0
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MovieViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return trailer.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTrailer", for: indexPath) as! TrailerCell
        cell.backgroundColor = UIColor.black
        
        print(trailerID[indexPath.row])
        
        cell.getVideo(videoCode: trailerID[indexPath.row])
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 200.0, height: tableView.frame.size.height/6)
    }
    
}
