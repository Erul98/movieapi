//
//  ViewController.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/14/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit
import SDWebImage

class ViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {

    @IBOutlet weak var tableview: UITableView!
    
    static var CheckID: Int = {
        return 0
    }()
    
    var refreshControl : UIRefreshControl = UIRefreshControl()
    
    static var idMVDetail: Int?
    static var checkMVDetail: Bool?
    
    var po:[MovieDetail] = [MovieDetail]()
    {
        didSet
        {
            tableview.reloadData()
        }
    }
    var now:[MovieDetail] = [MovieDetail]()
    {
        didSet
        {
            tableview.reloadData()
        }
    }
    var top:[MovieDetail] = [MovieDetail]()
    {
        didSet
        {
            tableview.reloadData()
        }
    }
    var up:[MovieDetail] = [MovieDetail]()
    {
        didSet
        {
            tableview.reloadData()
        }
    }
    var titleOfRows = ["Popular","Now Playing","Top Rated","Upcoming"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .black
        tableview.backgroundColor = .black
        tableview.separatorStyle = .none
        setupView()
        //Do any additional setup after loading the view.
        // get Api Popular
    }
    
    func AddPullToRefresh (){
        let attributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        
        refreshControl.attributedTitle = NSAttributedString(string: "", attributes: attributes)
        
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControl.Event.valueChanged)
        
        tableview.addSubview(refreshControl) // not required when using UITableViewController
    }
    
    @objc func refresh() {
        setupView()
        refreshControl.endRefreshing();
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        AddPullToRefresh()
        //tableview.reloadData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
//        self.po.removeAll()
//        self.top.removeAll()
//        self.now.removeAll()
//        self.up.removeAll()
        //self.dismiss(animated: true, completion: nil)
    }
    
    func setupView(){
        self.navigationItem.title = "VIDEO3ML"
        self.navigationController?.navigationBar.barStyle = UIBarStyle.blackTranslucent
        ViewController.checkMVDetail = false
        print(1)
        API().getJSONResponse(baseUrl: API.urlBasePo, param: API.getParamMovieList(page: 1)){
            rs in
            self.po.removeAll()
            for i in rs
            {
                //print(i.tittle)
                //print(i.backdrop_path)
                self.po.append(i)
            }
            print("a")
        }
        // get Api Now playing
        print(2)
        API().getJSONResponse(baseUrl: API.urlBaseNow, param: API.getParamMovieList(page: 1)){
            rs in
            self.now.removeAll()
            for i in rs
            {
                //print(i.tittle)
                self.now.append(i)
            }
            print("b")
        }
        // get Api Top rated
        print(3)
        API().getJSONResponse(baseUrl: API.urlBaseTop, param: API.getParamMovieList(page: 1)){
            rs in
            self.top.removeAll()
            for i in rs
            {
                //print(i.tittle)
                self.top.append(i)
            }
            print("c")
        }
        // get Api upcoming
        API().getJSONResponse(baseUrl: API.urlBaseUp, param: API.getParamMovieList(page: 1)){
            rs in
            self.up.removeAll()
            for i in rs
            {
                //print(i.tittle)
                self.up.append(i)
            }
        }
        tableview.allowsSelection = false
        tableview.delegate = self
        tableview.dataSource = self
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //print(iv.count)
//        return iv.count
        if titleOfRows.count > 0{
            return titleOfRows.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableview.dequeueReusableCell(withIdentifier: "cellPo", for: indexPath) as! cellPo
        
        cell.tableLabel.text = titleOfRows[indexPath.row]
        cell.collectionView.tag = indexPath.row
        cell.collectionView.dataSource = self
        cell.collectionView.delegate = self
        cell.collectionView.reloadData()
        cell.all.tag = indexPath.row
        cell.delegateList = self
        
        return cell
//        if iv.count > 0{
//            let earchIv = self.iv[indexPath.row]
//            cell.label?.text = earchIv.tittle
//            if let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.backdrop_path! as? String
//            {
//                print(stringiv)
//                cell.imageview?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
//            }
//
//        }
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let cell = cell as? cellPo{
            cell.collectionView.reloadData()
        }
    }
}

extension ViewController: UICollectionViewDataSource, UICollectionViewDelegate{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if (collectionView.tag == 0)
        {
            print(po.count)
            return po.count
        }
        else if (collectionView.tag == 1)
        {
            //print(now.count)
            return now.count
        }
        else if (collectionView.tag == 2)
        {
            //print(top.count)
            return top.count
        }
        else if (collectionView.tag == 3)
        {
            //print(up.count)
            return up.count
        }
        else
        {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellCV", for: indexPath) as! CollectionViewCell
        if (collectionView.tag == 0)
        {
            if po.count > 0{
                let earchIv = self.po[indexPath.row]
                cell.label?.text = earchIv.tittle
                if let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.poster_path! as? String
                {
                    print(stringiv)
                    cell.imageview?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
                }
                
            }
        }
        else if (collectionView.tag == 1)
        {
            if now.count > 0{
                let earchIv = self.now[indexPath.row]
                cell.label?.text = earchIv.tittle
                if let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.poster_path! as? String
                {
                    print(stringiv)
                    cell.imageview?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
                }
                
            }
        }
        else if (collectionView.tag == 2)
        {
            if top.count > 0{
                let earchIv = self.top[indexPath.row]
                cell.label?.text = earchIv.tittle
                if let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.poster_path! as? String
                {
                    print(stringiv)
                    cell.imageview?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
                }
                
            }
        }
        else if (collectionView.tag == 3)
        {
            if up.count > 0{
                let earchIv = self.up[indexPath.row]
                cell.label?.text = earchIv.tittle
                if let stringiv = "https://image.tmdb.org/t/p/w1280"+earchIv.poster_path! as? String
                {
                    print(stringiv)
                    cell.imageview?.sd_setImage(with: URL(string: stringiv), placeholderImage: UIImage(named: "meow.jpeg"))
                }
                
            }
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("abc")
        if collectionView.tag == 0
        {
            ViewController.idMVDetail = po[indexPath.row].id
            ViewController.checkMVDetail = true
            ListViewController.checkMVDetail = false
            print(ViewController.idMVDetail!)
            let mvc = self.storyboard?.instantiateViewController(withIdentifier: "moviedetail") as! MovieViewController
            self.present(mvc, animated: true, completion: nil)
        }
        else if collectionView.tag == 1
        {
            ViewController.idMVDetail = now[indexPath.row].id
            ViewController.checkMVDetail = true
            ListViewController.checkMVDetail = false
            print(ViewController.idMVDetail!)
            let mvc = self.storyboard?.instantiateViewController(withIdentifier: "moviedetail") as! MovieViewController
            self.present(mvc, animated: true, completion: nil)
        }
        else if collectionView.tag == 2
        {
            ViewController.idMVDetail = top[indexPath.row].id
            ViewController.checkMVDetail = true
            ListViewController.checkMVDetail = false
            print(ViewController.idMVDetail!)
            let mvc = self.storyboard?.instantiateViewController(withIdentifier: "moviedetail") as! MovieViewController
            self.present(mvc, animated: true, completion: nil)
        }
        else if collectionView.tag == 3
        {
            ViewController.idMVDetail = up[indexPath.row].id
            ViewController.checkMVDetail = true
            ListViewController.checkMVDetail = false
            print(ViewController.idMVDetail!)
            let mvc = self.storyboard?.instantiateViewController(withIdentifier: "moviedetail") as! MovieViewController
            self.present(mvc, animated: true, completion: nil)
        }
    }
    // Now Playing ----
}

extension ViewController: ListViewDelegate{
    func MovieList(check: Int) {
        print(check)
        ViewController.CheckID = check
        print(ViewController.CheckID)
        let lvc = self.storyboard?.instantiateViewController(withIdentifier: "listview") as! ListViewController
        self.navigationController?.pushViewController(lvc, animated: true)
    }
}
