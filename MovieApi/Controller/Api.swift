//
//  Api.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/14/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import Foundation
import Alamofire
class API {
    public static  var key = "c9238e9fff997ddc12fc76e3904e2618"
    public static var urlBase = "https://api.themoviedb.org/3"
    public static let urlBasePo = API.urlBase + "/movie/popular"
    public static let urlBaseNow = API.urlBase + "/movie/now_playing"
    public static let urlBaseUp = API.urlBase + "/movie/upcoming"
    public static let urlBaseTop = API.urlBase + "/movie/top_rated"
    
    init()
    {
        
    }
    
    static func getParamMovieList(page: Int) -> [String:Any]{
        return ["api_key": "\(API.key)","page":"\(page)"]
    }
    
    static func getURLMovieDetail (id : String)->String{
        return API.urlBase + "/movie/" + id
    }
    
    static func getDefaultParam() ->[String:Any]{
        return ["api_key": "\(API.key)"]
    }
    
    static func getTrailerKeyURL (id : String)->String{
        return API.urlBase + "/movie/" + id + "/videos"
    }
    
    func getJSONResponse(baseUrl: String,param: [String:Any],completion: @escaping([MovieDetail]) -> Void)
    {
        var moviesDetail = [MovieDetail]()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        manager.request(baseUrl, method: .get, parameters: param)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                if let json = response.result.value as? [String: Any]{
                    let results = json["results"] as! NSArray
                    for i in results
                    {
                        let t = i as! [String:Any]
                        if let item = MovieDetail(JSON: t){
                            moviesDetail.append(item)
                        }

                    }
                    completion(moviesDetail)
                }
                

        }
    }
    
    func getMovieDetailJSONResponse(baseUrl: String, param: [String:Any],completion: @escaping([ShowMovieDetail]) -> Void)
    {
        var moviesDetail = [ShowMovieDetail]()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        manager.request(baseUrl, method: .get, parameters: param)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                if let json = response.result.value as? [String: Any]{
                    print(json)
                    if let item = ShowMovieDetail(JSON: json)
                    {
                        moviesDetail.append(item)
                    }
                    completion(moviesDetail)
                }
        }
    }
    
    func getTrailerJSONResponse(baseUrl: String,param: [String:Any],completion: @escaping([ShowTrailerDetail]) -> Void)
    {
        var trailerDetail = [ShowTrailerDetail]()
        
        let manager = Alamofire.SessionManager.default
        manager.session.configuration.timeoutIntervalForRequest = 30
        
        manager.request(baseUrl, method: .get, parameters: param)
            .validate(statusCode: 200..<300)
            .responseJSON { response in
                
                if let json = response.result.value as? [String: Any]{
                    let results = json["results"] as! NSArray
                    for i in results
                    {
                        let t = i as! [String:Any]
                        if let item = ShowTrailerDetail(JSON: t){
                            trailerDetail.append(item)
                        }
                        
                    }
                    completion(trailerDetail)
                }
                
                
        }
    }
}
