//
//  cellList.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/21/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit
import Cosmos

class cellList: UITableViewCell {
    
    @IBOutlet weak var img: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var vote: UILabel!
    @IBOutlet weak var cosmos: CosmosView!
    @IBOutlet weak var voteStar: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        cosmos.settings.updateOnTouch = false
        backgroundColor = .black
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
