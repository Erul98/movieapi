//
//  cellPo.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/17/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit

protocol ListViewDelegate: class {
    func MovieList(check: Int)
}

class cellPo: UITableViewCell {
    
    weak var delegateList: ListViewDelegate?
    
    @IBOutlet weak var tableLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var all: UIButton!
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        // code common to all your cells goes here
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    @IBAction func allList(_ sender: UIButton) {
        delegateList?.MovieList(check: sender.tag)
    }
    
}
