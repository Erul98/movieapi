//
//  InfoCell.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 8/4/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit

class InfoCell: UITableViewCell {

    @IBOutlet weak var imgInfoCell: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
