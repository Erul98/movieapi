//
//  cellList_1.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 8/5/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit

class cellList_1: UITableViewCell {

    @IBOutlet weak var textCell: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textCell.isEditable = false
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
