//
//  CollectionViewCell.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/17/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageview: UIImageView!
    
    @IBOutlet weak var label: UILabel!
    
}
