//
//  TrailerCell.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 8/9/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import UIKit
import YoutubePlayer_in_WKWebView

class TrailerCell: UICollectionViewCell {
    @IBOutlet weak var trailer: WKYTPlayerView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    func getVideo(videoCode : String){
        trailer.load(withVideoId: videoCode)
        //webView.loadRequest(URLRequest(url: url!))
    }
}
