//
//  Model.swift
//  MovieApi
//
//  Created by Nguyen Hoang Anh on 7/14/19.
//  Copyright © 2019 Nguyen Hoang Anh. All rights reserved.
//

import Foundation
import ObjectMapper
class MovieDetail: Mappable{
    var image : String?
    var tittle : String?
    var release_date : String?
    var id  : Int?
    var vote_average : Double?
    var overview : String?
    var budget : Int?
    var homepage : String?
    var revenue : Int?
    var runtime : Int?
    var backdrop_path : String?
    var vote_count : Int?
    var poster_path: String?
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        vote_count <- map["vote_count"]
        image <- map["poster_path"]
        tittle <- map["title"]
        release_date <- map["release_date"]
        id <- map["id"]
        vote_average <- map["vote_average"]
        overview <- map["overview"]
        revenue <- map["revenue"]
        runtime <- map["runtime"]
        budget <- map["budget"]
        homepage <- map["homepage"]
        backdrop_path <- map["backdrop_path"]
        poster_path <- map["poster_path"]
    }
}

class ShowMovieDetail: Mappable{
    
    var image: String?
    var overview: String?
    
    required init?(map: Map) {
        
    }
    
    init() {
        
    }
    
    func mapping(map: Map) {
        image <- map["poster_path"]
        overview <- map["overview"]
    }
}

class ShowTrailerDetail: Mappable{
    var id : String?
    var iso_639_1 : String?
    var iso_3166_1 : String?
    var key  : String?
    var name : String?
    var site : String?
    var size : Int?
    var type : String?
    
    
    required init?(map: Map){
        
    }
    init() {
        
    }
    func mapping(map: Map) {
        id <- map["id"]
        iso_639_1 <- map["iso_639_1"]
        iso_3166_1 <- map["iso_3166_1"]
        
        key <- map["key"]
        name <- map["name"]
        site <- map["site"]
        size <- map["size"]
        type <- map["type"]
    }
}
